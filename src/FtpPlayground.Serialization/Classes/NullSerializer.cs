﻿namespace FtpPlayground.Serialization
{
    /// <summary>
    /// Empty implementation of <see cref="ISerializer"/> useful for testing purposes.
    /// </summary>
    public class NullSerializer : ISerializer
    {
        public byte[] Serialize<T>(T value)
            where T : class, new()
        {
            return new byte[0];
        }

        public T Deserialize<T>(byte[] buffer)
            where T : class, new()
        {
            return default(T);
        }
    }
}
