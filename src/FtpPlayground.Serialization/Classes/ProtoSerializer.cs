﻿using ProtoBuf;
using System;
using System.IO;

namespace FtpPlayground.Serialization
{
    /// <summary>
    /// Implementation of <see cref="ISerializer"/> using ProtoBuf.
    /// </summary>
    public class ProtoSerializer : ISerializer
    {
        public byte[] Serialize<T>(T value)
            where T : class, new()
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            byte[] buffer = null;

            using (var stream = new MemoryStream())
            {
                Serializer.Serialize(stream, value);
                buffer = stream.ToArray();
            }

            return buffer;
        }

        public T Deserialize<T>(byte[] buffer)
            where T : class, new()
        {
            using (var stream = new MemoryStream(buffer))
            {
                return Serializer.Deserialize<T>(stream);
            }
        }
    }
}
