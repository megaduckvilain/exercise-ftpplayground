﻿namespace FtpPlayground.Serialization
{
    /// <summary>
    /// Simple serialization interface used to abstract from direct dependencies on any given serialization framework.
    /// </summary>
    public interface ISerializer
    {
        byte[] Serialize<T>(T value) 
            where T : class, new();

        T Deserialize<T>(byte[] buffer) 
            where T : class, new();
    }
}
