﻿using System;
using System.Collections.Generic;

namespace FtpPlayground.Caching
{
    /// <summary>
    /// Basic cache client interface that follows Microsoft's <see cref="ObjectCache"/> implementation.
    /// </summary>
    /// <remarks>
    /// 
    /// </remarks>
    public interface ICacheClient : IDisposable
    {        
        bool Add(string key, byte[] value);
        
        byte[] AddOrGetExisting(string key, byte[] value);

        bool AddOrSet(string key, byte[] value);

        bool Contains(string key);
                
        byte[] Get(string key);

        IDictionary<string, byte[]> GetValues(IEnumerable<string> keys);
       
        byte[] Remove(string key);
                
        void Set(string key, byte[] value);       
    }
}
