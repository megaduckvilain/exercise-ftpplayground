﻿using System;

namespace FtpPlayground.Caching
{
    /// <summary>
    /// Factory pattern implementation for <see cref="ICacheClient"/>.
    /// </summary>
    public interface ICacheClientFactory : IDisposable
    {
        ICacheClient GetCacheClient();
    }
}
