﻿namespace FtpPlayground.Caching
{
    /// <summary>
    /// Empty implementation of <see cref="ICacheClientFactory"/> useful for testing purposes.
    /// </summary>
    public sealed class NullCacheClientFactory : ICacheClientFactory
    {
        public ICacheClient GetCacheClient()
        {
            return new NullCacheClient();
        }

        public void Dispose()
        {
            // Do nothing
        }
    }
}
