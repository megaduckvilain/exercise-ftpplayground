﻿using System.Collections.Generic;

namespace FtpPlayground.Caching
{
    /// <summary>
    /// Empty implementation of <see cref="ICacheClient"/> useful for testing purposes.
    /// </summary>
    public class NullCacheClient : ICacheClient
    {
        public bool Add(string key, byte[] value)
        {
            return true;
        }

        public byte[] AddOrGetExisting(string key, byte[] value)
        {
            return new byte[0];
        }

        public bool AddOrSet(string key, byte[] value)
        {
            return true;
        }

        public bool Contains(string key)
        {
            return true;
        }

        public byte[] Get(string key)
        {
            return new byte[0];
        }

        public IDictionary<string, byte[]> GetValues(IEnumerable<string> keys)
        {
            return new Dictionary<string, byte[]> { };
        }

        public byte[] Remove(string key)
        {
            return new byte[0];
        }

        public void Set(string key, byte[] value)
        {
            // Do nothing
        }

        public void Dispose()
        {
            // Do nothing
        }
    }
}
