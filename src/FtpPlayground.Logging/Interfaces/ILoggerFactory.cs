﻿namespace FtpPlayground.Logging
{
    /// <summary>
    /// Factory pattern implementation for <see cref="ILogger"/>.
    /// </summary>
    public interface ILoggerFactory
    {
        ILogger GetLogger(string name);
    }
}
