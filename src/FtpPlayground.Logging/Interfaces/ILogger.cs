﻿using System;

namespace FtpPlayground.Logging
{
    /// <summary>
    /// Basic logger interface that follows Apache's log4net standard.
    /// </summary>
    /// <remarks>
    /// https://logging.apache.org/log4net/
    /// </remarks>
    public interface ILogger
    {
        void Debug(string message);

        void Info(string message);

        void Warn(string message);

        void Error(string message);

        void Error(Exception ex);

        void Error(string message, Exception ex);

        void Fatal(string message);

        void Fatal(Exception ex);

        void Fatal(string message, Exception ex);
    }
}
