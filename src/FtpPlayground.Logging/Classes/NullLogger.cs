﻿using System;

namespace FtpPlayground.Logging
{
    /// <summary>
    /// Empty implementation of <see cref="ILogger"/> useful for testing purposes.
    /// </summary>
    public class NullLogger : ILogger
    {
        public void Debug(string message)
        {
            // Do nothing
        }

        public void Error(string message)
        {
            // Do nothing
        }

        public void Error(Exception ex)
        {
            // Do nothing
        }

        public void Error(string message, Exception ex)
        {
            // Do nothing
        }

        public void Fatal(string message)
        {
            // Do nothing
        }

        public void Fatal(Exception ex)
        {
            // Do nothing
        }

        public void Fatal(string message, Exception ex)
        {
            // Do nothing
        }

        public void Info(string message)
        {
            // Do nothing
        }

        public void Warn(string message)
        {
            // Do nothing
        }
    }
}
