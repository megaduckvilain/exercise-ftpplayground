﻿namespace FtpPlayground.Logging
{
    /// <summary>
    /// Empty implementation of <see cref="ILoggerFactory"/> useful for testing purposes.
    /// </summary>
    public class NullLoggerFactory : ILoggerFactory
    {
        public ILogger GetLogger(string name)
        {
            return new NullLogger();
        }
    }
}
