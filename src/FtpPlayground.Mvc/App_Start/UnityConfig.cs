using FtpPlayground.Caching;
using FtpPlayground.Logging;
using FtpPlayground.Serialization;
using System;
using Unity;
using Unity.Lifetime;

namespace FtpPlayground.Mvc
{
    public static class UnityConfig
    {        
        private static Lazy<IUnityContainer> _container = new Lazy<IUnityContainer>(() =>
         {
             var container = new UnityContainer();

             RegisterTypes(container);

             return container;
         });
                
        public static IUnityContainer Container => _container.Value;
                
        /// <summary>
        /// Configures the container.
        /// </summary>
        /// <param name="container"></param>
        public static void RegisterTypes(IUnityContainer container)
        {
            // Note: 
            // For the sake of simplicity I've registered all types in code rather than load registration configurations
            // from web.config file.
            //
            // It's worth noting here that majority of our dependecies are registered with the SingletonLifetimeManager which
            // makes our life alot simpler in terms of implementation of those dependencies.
            // At the same time it gives us thread safe access to those resources.
                        
            // Register dependencies in our container                        
            container.RegisterType<ILoggerFactory, FtpPlayground.Logging.NullLoggerFactory>(new SingletonLifetimeManager());
            container.RegisterType<ICacheClientFactory, FtpPlayground.Caching.Memory.CacheClientFactory>(new SingletonLifetimeManager());
            container.RegisterType<ISerializer, FtpPlayground.Serialization.ProtoSerializer>(new SingletonLifetimeManager());                        
        }
    }
}