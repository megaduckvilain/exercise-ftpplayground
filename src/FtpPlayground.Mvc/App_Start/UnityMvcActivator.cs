using System.Linq;
using System.Web.Mvc;
using Unity.AspNet.Mvc;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(FtpPlayground.Mvc.UnityMvcActivator), nameof(FtpPlayground.Mvc.UnityMvcActivator.Start))]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(FtpPlayground.Mvc.UnityMvcActivator), nameof(FtpPlayground.Mvc.UnityMvcActivator.Shutdown))]

namespace FtpPlayground.Mvc
{
    public static class UnityMvcActivator
    {
        public static void Start()
        {
            FilterProviders.Providers.Remove(FilterProviders.Providers.OfType<FilterAttributeFilterProvider>().First());
            FilterProviders.Providers.Add(new UnityFilterAttributeFilterProvider(UnityConfig.Container));

            DependencyResolver.SetResolver(new UnityResolver(UnityConfig.Container));
        }

        public static void Shutdown()
        {
            (DependencyResolver.Current as UnityResolver).Dispose();
        }
    }
}