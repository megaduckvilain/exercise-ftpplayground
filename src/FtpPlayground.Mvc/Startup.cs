﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(FtpPlayground.Mvc.Startup))]
namespace FtpPlayground.Mvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure components            
            appBuilder.ConfigureSignalR();            
        }
    }
}
