﻿using FtpPlayground.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web.Mvc;
using Unity;

[ExcludeFromCodeCoverage]
public class UnityResolver : IDependencyResolver, IDisposable
{
    #region >>>> IDisposable

    private bool _disposed = false;

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    public void Dispose(bool disposing)
    {
        if (_disposed)
        {
            return;
        }

        if (disposing)
        {
            Container.Dispose();
        }

        _disposed = true;
    }

    #endregion

    ILoggerFactory _loggerFactory;
    ILogger _logger;

    public IUnityContainer Container { get; private set; }
        
    public UnityResolver(IUnityContainer container)
    {
        if (container == null)
        {
            throw new ArgumentNullException(nameof(container));
        }

        Container = container;

        // NOTE: In order to be able to log resolution errors we have to resolve a logger component here
        _loggerFactory = container.Resolve<ILoggerFactory>();

        if (_loggerFactory != null)
        {
            _logger = _loggerFactory.GetLogger(nameof(UnityResolver));
        }
    }
        
    public T GetService<T>(string name)
    {
        try
        {
            return (T)GetService(typeof(T), name);
        }
#pragma warning disable CS0168 // Variable is declared but never used
        catch (Exception ex)
#pragma warning restore CS0168 // Variable is declared but never used
        {
            return default(T);
        }
    }

    public object GetService(Type serviceType, string name)
    {
        try
        {
            return Container.Resolve(serviceType, name);
        }
#pragma warning disable CS0168 // Variable is declared but never used
        catch (Exception ex)
#pragma warning restore CS0168 // Variable is declared but never used
        {
            if (_logger != null)
            {
                _logger.Fatal($"{nameof(UnityResolver.GetService)} Unable to resolve component. Type: {serviceType?.Name ?? "NULL"}; Name: {name ?? "NULL"}; Exception: {ex.ToString()};");
            }

            return null;
        }
    }

    public T GetService<T>()
    {
        try
        {
            return (T)GetService(typeof(T));
        }
#pragma warning disable CS0168 // Variable is declared but never used
        catch (Exception ex)
#pragma warning restore CS0168 // Variable is declared but never used
        {
            return default(T);
        }
    }

    public object GetService(Type serviceType)
    {
        try
        {
            return Container.Resolve(serviceType);
        }
#pragma warning disable CS0168 // Variable is declared but never used
        catch (Exception ex)
#pragma warning restore CS0168 // Variable is declared but never used
        {
            if (_logger != null)
            {
                _logger.Fatal($"{nameof(UnityResolver.GetService)} Unable to resolve component. Type: {serviceType?.Name ?? "NULL"}; Exception: {ex.ToString()};");
            }

            return null;
        }
    }

    public IEnumerable<T> GetServices<T>()
    {
        try
        {
            return GetServices(typeof(T))
                .Cast<T>();
        }
#pragma warning disable CS0168 // Variable is declared but never used
        catch (Exception ex)
#pragma warning restore CS0168 // Variable is declared but never used
        {
            return default(IEnumerable<T>);
        }
    }

    public IEnumerable<object> GetServices(Type serviceType)
    {
        try
        {
            return Container.ResolveAll(serviceType);
        }
#pragma warning disable CS0168 // Variable is declared but never used
        catch (Exception ex)
#pragma warning restore CS0168 // Variable is declared but never used
        {
            if (_logger != null)
            {
                _logger.Fatal($"{nameof(UnityResolver.GetServices)} Unable to resolve component. Type: {serviceType?.Name ?? "NULL"}; Exception: {ex.ToString()};");
            }

            return null;
        }
    }
}