﻿using System;
using System.Runtime.Serialization;

namespace FtpPlayground.Mvc.Models
{
    [DataContract]
    public class ExplorerGridItem
    {
        /// <summary>
        /// Sequential id of the item.
        /// </summary>
        [DataMember(Order = 1)]
        public int Id { get; set; }

        /// <summary>
        /// Name of file or directory.
        /// </summary>
        [DataMember(Order = 2)]
        public string FileName { get; set; }

        /// <summary>
        /// True if current item appears to be a directory link otherwise False.
        /// </summary>
        [DataMember(Order = 3)]
        public bool IsDirectory { get; set; }

        /// <summary>
        /// Download id of the item.
        /// </summary>
        [DataMember(Order = 4)]
        public Guid? DownloadId { get; set; }
    }
}