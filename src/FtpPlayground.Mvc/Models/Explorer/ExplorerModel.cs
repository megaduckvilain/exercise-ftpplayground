﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FtpPlayground.Mvc.Models
{
    /// <summary>
    /// Model for Explorer page.
    /// </summary>
    /// <remarks>
    /// In order to store this model in cache we need to decorate all model members with attributes that will be used by the chosen serialization framework.
    /// </remarks>
    [DataContract]
    public class ExplorerModel
    {
        [DataMember(Order = 1)]
        public List<ExplorerGridItem> GridItems { get; set; }
    }
}