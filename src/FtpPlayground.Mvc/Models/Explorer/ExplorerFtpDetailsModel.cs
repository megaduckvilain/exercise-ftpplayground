﻿using System.Runtime.Serialization;

namespace FtpPlayground.Mvc.Models
{
    [DataContract]
    public class ExplorerFtpDetailsModel
    {
        [DataMember(Order = 1)]
        public string FtpUri { get; set; }

        [DataMember(Order = 2)]
        public string FtpUsername { get; set; }

        [DataMember(Order = 3)]
        public string FtpPassword { get; set; }
    }
}