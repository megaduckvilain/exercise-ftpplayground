﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Infrastructure;
using System;

namespace FtpPlayground.Mvc
{
    public class DownloadManagerHubClient : IDownloadManagerHubClient
    {
        private readonly IConnectionManager _connectionManager;
        private readonly IHubContext _context;

        public DownloadManagerHubClient(IConnectionManager connectionManager)
        {
            if (connectionManager == null)
            {
                throw new ArgumentNullException(nameof(connectionManager));
            }

            _connectionManager = connectionManager;
            _context = connectionManager.GetHubContext<DownloadManagerHub>();
        }

        public void DownloadProgress(string downloadId, string downloadRate, string downloadSize, int downloadProgress, string downloadTime)
        {
            dynamic target = _context.Clients.All;

            target.downloadProgress(downloadId, downloadRate, downloadSize, downloadProgress, downloadTime);
        }

        public void DownloadCompleted(string downloadId, string downloadTime, string filePath, string error)
        {
            dynamic target = _context.Clients.All;

            target.downloadCompleted(downloadId, downloadTime, filePath, error);
        }

        public void DownloadCancelled(string downloadId, string filePath)
        {
            dynamic target = _context.Clients.All;

            target.downloadCancelled(downloadId, filePath);
        }
    }
}