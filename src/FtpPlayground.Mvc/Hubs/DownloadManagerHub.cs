﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;

namespace FtpPlayground.Mvc
{
    [HubName("DownloadManagerHub")]
    public class DownloadManagerHub : Hub
    {
        public void DownloadProgress(string downloadId, string downloadRate, string downloadSize, int downloadProgress, string downloadTime)
        {
            var request = new
            {
                downloadId = downloadId,
                downloadRate = downloadRate,
                downloadSize = downloadSize,
                downloadProgress = downloadProgress,
                downloadTime = downloadTime
            };

            Clients.Caller.downloadProgress(JsonConvert.SerializeObject(request));
        }

        public void DownloadCompleted(string downloadId, string downloadTime, string filePath, string error)
        {
            var request = new
            {
                downloadId = downloadId,
                downloadTime = downloadTime,
                filePath = filePath,
                error = error
            };

            Clients.Caller.downloadCompleted(JsonConvert.SerializeObject(request));
        }

        public void DownloadCancelled(string downloadId, string filePath)
        {
            var request = new
            {
                downloadId = downloadId,
                filePath = filePath
            };

            Clients.Caller.downloadCancelled(JsonConvert.SerializeObject(request));
        }
    }
}