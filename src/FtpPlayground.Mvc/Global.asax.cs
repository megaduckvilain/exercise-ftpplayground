﻿using FtpPlayground.DownloadManager;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Unity.Lifetime;

namespace FtpPlayground.Mvc
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Additional container registrations

            // Note: Some registrations require access to HttpContext so we have to do them after the pipeline is setup
            var workingDirectoryPath = HttpContext.Current.Server.MapPath("~/App_Data/DownloadManager");

            UnityConfig.Container.RegisterInstance(
                typeof(IDownloadManager), 
                null, 
                new FtpPlayground.DownloadManager.DownloadManager(workingDirectoryPath, TimeSpan.FromMilliseconds(500)),
                new SingletonLifetimeManager());
        }
    }
}
