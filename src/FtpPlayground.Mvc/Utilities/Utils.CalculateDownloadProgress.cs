﻿using System;

namespace FtpPlayground.Mvc.Utilities
{
    public static partial class Utils
    {
        public static int CalculateDownloadProgress(double downloadedBytes, double fileSize)
        {
            if (downloadedBytes <= 0)
            {
                return 0;
            }

            if (downloadedBytes >= fileSize)
            {
                return 100;
            }

            return (int)((downloadedBytes / fileSize) * 100D);
        }
    }
}