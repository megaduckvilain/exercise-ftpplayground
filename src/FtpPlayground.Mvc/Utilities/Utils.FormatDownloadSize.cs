﻿using ByteSizeLib;

namespace FtpPlayground.Mvc.Utilities
{
    public  static partial class Utils
    {
        public static string FormatDownloadSize(double downloadedBytes, double fileSize)
        {
            return $"{ByteSize.FromBytes(downloadedBytes).ToString()} of {ByteSize.FromBytes(fileSize).ToString()}"; 
        }
    }
}