﻿using ByteSizeLib;

namespace FtpPlayground.Mvc.Utilities
{
    public  static partial class Utils
    {
        public static string FormatDownloadRate(double rate)
        {
            return $"{ByteSize.FromBytes(rate).ToString("0.#")}/s";
        }
    }
}