﻿using ByteSizeLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FtpPlayground.Mvc.Utilities
{
    public  static partial class Utils
    {
        public static string FormatDownloadTime(TimeSpan time)
        {                        
            var result = "";

            // More that 
            if (time.TotalHours >= 1D)
            {
                result = $"{result}{time.Hours:n0} hours, {time.Minutes:n0} minutes, {time.Seconds:n0} seconds";
            }
            else if (time.TotalMinutes >= 1D)
            {
                result = $"{result}{time.Minutes:n0} minutes, {time.Seconds:n0} seconds";
            }
            else
            {
                result = $"{result}{time.TotalSeconds:n0} seconds";                
            }

            return result;
        }
    }
}