﻿using FtpPlayground.Caching;
using FtpPlayground.Logging;
using FtpPlayground.Serialization;
using System;
using System.Web.Mvc;

namespace FtpPlayground.Mvc.Controllers
{
    /// <summary>
    /// Base controller class that handless basic initialization and other boilerplate functionality.
    /// </summary>
    public abstract class AbstractController : Controller
    {
        protected ILogger Logger { get; private set; }

        protected ICacheClient CacheClient { get; private set; }

        protected ISerializer Serializer { get; private set; }

        public AbstractController(
            ILoggerFactory loggerFactory,
            ICacheClientFactory cacheClientFactory,
            ISerializer serializer)
        {
            // Validate parameters
            if (loggerFactory == null)
            {
                throw new ArgumentNullException(nameof(loggerFactory));
            }

            if (cacheClientFactory == null)
            {
                throw new ArgumentNullException(nameof(cacheClientFactory));
            }

            if (serializer == null)
            {
                throw new ArgumentNullException(nameof(serializer));
            }

            // Init properties
            Logger = loggerFactory.GetLogger(GetType().Name);
            CacheClient = cacheClientFactory.GetCacheClient();
            Serializer = serializer;
        }
    }
}