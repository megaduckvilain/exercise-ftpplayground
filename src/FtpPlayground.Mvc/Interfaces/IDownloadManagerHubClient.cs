﻿namespace FtpPlayground.Mvc
{
    /// <summary>
    /// Client interface for <see cref="DownloadManagerHub"/>.
    /// </summary>
    public interface IDownloadManagerHubClient
    {
        void DownloadProgress(string downloadId, string downloadRate, string downloadSize, int downloadProgress, string downloadTime);

        void DownloadCompleted(string downloadId, string downloadTime, string filePath, string error);

        void DownloadCancelled(string downloadId, string filePath);
    }
}