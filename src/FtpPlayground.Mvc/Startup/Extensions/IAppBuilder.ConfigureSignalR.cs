﻿using Microsoft.AspNet.SignalR;
using Owin;
using System.Web.Mvc;
using Unity.Lifetime;

namespace FtpPlayground.Mvc
{
    internal static partial class AppBuilder_Extensions
    {
        internal static void ConfigureSignalR(this IAppBuilder appBuilder)
        {
            appBuilder.MapSignalR();

            var resolver = DependencyResolver.Current as UnityResolver;

            resolver.Container.RegisterInstance(typeof(IDownloadManagerHubClient), null, new DownloadManagerHubClient(GlobalHost.ConnectionManager), new SingletonLifetimeManager());
        }
    }
}
