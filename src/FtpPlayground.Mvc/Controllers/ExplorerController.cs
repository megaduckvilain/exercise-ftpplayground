﻿using FtpPlayground.Caching;
using FtpPlayground.DownloadManager;
using FtpPlayground.Logging;
using FtpPlayground.Mvc.Models;
using FtpPlayground.Mvc.Utilities;
using FtpPlayground.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Unity.Attributes;

namespace FtpPlayground.Mvc.Controllers
{
    public class ExplorerController : AbstractController
    {
        /// <summary>
        /// Key used to store model in the cache.
        /// </summary>
        private readonly string _sessionCacheKeyModel;

        /// <summary>
        /// Key used to store FTP details in the cache.
        /// </summary>
        private readonly string _sessionCacheKeyFtpModel;

        /// <summary>
        /// Manager class used for downloading files in the background.
        /// </summary>
        private IDownloadManager _downloadManager;

        /// <summary>
        /// SignalR hub client used to send notification messages to the browser.
        /// </summary>
        private IDownloadManagerHubClient _downloadManagerHubClient;

        public ExplorerController(
            ILoggerFactory loggerFactory,
            ICacheClientFactory cacheClientFactory,
            ISerializer serializer)
            : base(loggerFactory, cacheClientFactory, serializer)
        {
            // TODO: 
            // Move key initialization logic into Initialize() method and add user information to it.
            // This will allow us to store sessions per user rather than share one among all users. 
            _sessionCacheKeyModel = $"session-model-{nameof(ExplorerController)}";
            _sessionCacheKeyFtpModel = $"session-ftp-{nameof(ExplorerController)}";
        }

        /// <summary>
        /// We use an injection method for some dependecies which are not used across all controllers. This helps keep our <see cref="AbstractController"/> constructor a bit cleaner.
        /// </summary>
        /// <param name="downloadManager"></param>
        [InjectionMethod]
        public void InjectionMethod(
            IDownloadManager downloadManager,
            IDownloadManagerHubClient downloadManagerHubClient)
        {
            if (downloadManager == null)
            {
                throw new ArgumentNullException(nameof(downloadManager));
            }

            if (downloadManagerHubClient == null)
            {
                throw new ArgumentNullException(nameof(downloadManagerHubClient));
            }

            _downloadManager = downloadManager;
            _downloadManagerHubClient = downloadManagerHubClient;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> LoadGrid(string ftpUri, string ftpUsername, string ftpPassword)
        {
            // Validate parameters
            if (string.IsNullOrWhiteSpace(ftpUri))
            {
                throw new ArgumentNullException(nameof(ftpUri));
            }

            if (string.IsNullOrWhiteSpace(ftpUsername))
            {
                throw new ArgumentNullException(nameof(ftpUsername));
            }

            if (string.IsNullOrWhiteSpace(ftpPassword))
            {
                throw new ArgumentNullException(nameof(ftpPassword));
            }

            try
            {
                var ftpDirectoryListing = "";

                #region >>>> Fetch data from FTP server

                // Note: This call will throw if the uri we passed in is invalid or not absolute,
                // doing it like this simplifies our error checks and keeps the flow a little cleaner.
                var requestUri = new Uri(ftpUri, UriKind.Absolute);

                // Init request
                var request = WebRequest.Create(ftpUri) as FtpWebRequest;
                request.Credentials = new NetworkCredential(ftpUsername, ftpPassword);

                // Set the command we want to execute on the ftp server. In this case we are interested in the directory listing.
                request.Method = WebRequestMethods.Ftp.ListDirectory;

                // Query server using an asynchronous call to get the response
                using (var response = await request.GetResponseAsync() as FtpWebResponse)
                {
                    // Check response
                    if (response == null)
                    {
                        throw new InvalidOperationException($"FTP server response is null.");
                    }

                    if (!(response.StatusCode == FtpStatusCode.OpeningData ||
                        response.StatusCode == FtpStatusCode.ClosingData))
                    {
                        throw new InvalidOperationException($"FTP server response does not indicate success. Message: {response.StatusDescription}");
                    }

                    // Process response stream                    
                    using (var stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            ftpDirectoryListing = await reader.ReadToEndAsync();
                        }
                    }
                }
                #endregion

                #region >>>> Init models

                var ftpModel = new ExplorerFtpDetailsModel
                {
                    FtpUri = ftpUri,
                    FtpUsername = ftpUsername,
                    FtpPassword = ftpPassword,
                };

                var model = new ExplorerModel
                {
                    GridItems = new List<ExplorerGridItem> { }
                };

                // Check response body we got from the ftp server and fill our model
                if (!string.IsNullOrWhiteSpace(ftpDirectoryListing))
                {
                    // Response body contains directory listing separated by carriage returns. So we need to 
                    // split it into individual lines.
                    var ftpDirectoryItems = ftpDirectoryListing.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                    if (ftpDirectoryItems.Length > 0)
                    {
                        var itemId = 1;

                        model.GridItems = ftpDirectoryItems
                            .Select(x =>
                            {
                                var item = new ExplorerGridItem
                                {
                                    Id = itemId,
                                    FileName = x,

                                    // Note: Here we try to guess if the file name returned by the server is in fact a directory link.                                     
                                    IsDirectory = !x.Contains("."),
                                };

                                itemId++;

                                return item;
                            })
                            .OrderByDescending(x => x.IsDirectory)
                            .ToList();
                    }
                }

                #endregion

                #region >>>> Store models in cache

                // Note: 
                // Before we can store our models in cache we have to serialize it. Here we use our serialization abstraction to 
                // do just that.
                // Advantage of doing it this way is that our application is generally oblivious to the underlying serialization protocol that
                // we decide to use. Should we change it in the future there will be no need to change the code.
                CacheClient.AddOrSet(_sessionCacheKeyModel, Serializer.Serialize(model));
                CacheClient.AddOrSet(_sessionCacheKeyFtpModel, Serializer.Serialize(ftpModel));

                #endregion

                // Render our view and return the result
                if (Request.IsAjaxRequest())
                {
                    return PartialView("_Grid", model);
                }

                return View(model);
            }
            catch (Exception ex)
            {
                // Log
                Logger.Error($"{nameof(ExplorerController.LoadGrid)}: Could not load grid.", ex);

                // Re-throw the exception so it can get handled via the MVC pipeline
                throw;
            }
        }

        [HttpGet]
        public ActionResult DownloadFile(int fileId)
        {
            try
            {
                #region >>>> Load models from cache

                // Note:
                // Usually we would wrap this logic in a convinient generic extension method and avoid the pain of having to write everything
                // out like this. 

                var modelBuffer = CacheClient.Get(_sessionCacheKeyModel);

                if (modelBuffer == null)
                {
                    throw new InvalidOperationException($"Could not load {nameof(ExplorerModel)} from cache.");
                }

                var model = Serializer.Deserialize<ExplorerModel>(modelBuffer);

                if (model == null)
                {
                    throw new InvalidOperationException($"Could not deserialize {nameof(ExplorerModel)}.");
                }

                var ftpModelBuffer = CacheClient.Get(_sessionCacheKeyFtpModel);

                if (ftpModelBuffer == null)
                {
                    throw new InvalidOperationException($"Could not load {nameof(ExplorerFtpDetailsModel)} from cache.");
                }

                var ftpModel = Serializer.Deserialize<ExplorerFtpDetailsModel>(ftpModelBuffer);

                if (ftpModel == null)
                {
                    throw new InvalidOperationException($"Could not deserialize {nameof(ExplorerFtpDetailsModel)}.");
                }

                #endregion

                #region >>>> Get file details from cache

                if (model.GridItems == null || !model.GridItems.Any())
                {
                    throw new InvalidOperationException($"Could not find file. {nameof(ExplorerModel.GridItems)} collection is null or empty.");
                }

                // Here we want to find a file that matches the id we recieved
                var gridItem = model.GridItems
                    .FirstOrDefault(x => x.Id == fileId);

                if (gridItem == null)
                {
                    throw new InvalidOperationException($"Could not find file with id {fileId}.");
                }

                #endregion

                // Start file download
                var downloadId = _downloadManager.StartDownload(ftpModel.FtpUri, ftpModel.FtpUsername, ftpModel.FtpPassword, gridItem.FileName, DownloadProgressHandler, DownloadCompletedHandler);

                // Update file details with the download id
                gridItem.DownloadId = downloadId;

                #region >>>> Update models in cache

                CacheClient.AddOrSet(_sessionCacheKeyModel, Serializer.Serialize(model));

                #endregion 

                // Return download id to the client

                // Note: 
                // Storing dowload id on the client will help us improve performance since we can just send updates
                // to UI from our progress tracking callbacks.
                // Otherwise we would need to translate them into file ids and that would require looking at the models in cache.
                var response = new
                {
                    fileId = fileId,
                    downloadId = downloadId
                };

                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Log
                Logger.Error($"{nameof(ExplorerController.LoadGrid)}: Could not load grid.", ex);

                // Re-throw the exception so it can get handled via the MVC pipeline
                throw;
            }
        }

        [NonAction]
        private void DownloadProgressHandler(Guid downloadId, double downloadRatePerSecond, double downloadedBytes, double fileSize, TimeSpan timeRemaining)
        {
            var formattedDownloadRate = Utils.FormatDownloadRate(downloadRatePerSecond);
            var formattedDownloadSize = Utils.FormatDownloadSize(downloadedBytes, fileSize);
            var formattedDownloadTime = Utils.FormatDownloadTime(timeRemaining);

            var downloadProgress = Utils.CalculateDownloadProgress(downloadedBytes, fileSize);

            _downloadManagerHubClient.DownloadProgress(downloadId.ToString(), formattedDownloadRate, formattedDownloadSize, downloadProgress, formattedDownloadTime);
        }

        [NonAction]
        private void DownloadCompletedHandler(Guid downloadId, TimeSpan timeTaken, string filePath, bool hasError, Exception errorDetails)
        {
            var formattedDownloadTime = Utils.FormatDownloadTime(timeTaken);

            var formattedError = hasError
                ? "Error downloading file!"
                : string.Empty;

            _downloadManagerHubClient.DownloadCompleted(downloadId.ToString(), formattedDownloadTime, filePath, formattedError);
        }

        [NonAction]
        private void DownloadCancelledHandler(Guid downloadId, string filePath)
        {            
            _downloadManagerHubClient.DownloadCancelled(downloadId.ToString(), filePath);
        }
    }
}