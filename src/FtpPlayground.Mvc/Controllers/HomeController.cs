﻿using FtpPlayground.Caching;
using FtpPlayground.Logging;
using FtpPlayground.Serialization;
using System.Web.Mvc;

namespace FtpPlayground.Mvc.Controllers
{
    public class HomeController : AbstractController
    {
        public HomeController(
            ILoggerFactory loggerFactory,
            ICacheClientFactory cacheClientFactory,
            ISerializer serializer)
            : base(loggerFactory, cacheClientFactory, serializer)
        {
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}