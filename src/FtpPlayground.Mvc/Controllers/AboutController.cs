﻿using FtpPlayground.Caching;
using FtpPlayground.Logging;
using FtpPlayground.Serialization;
using System.Web.Mvc;

namespace FtpPlayground.Mvc.Controllers
{
    public class AboutController : AbstractController
    {
        public AboutController(
            ILoggerFactory loggerFactory,
            ICacheClientFactory cacheClientFactory,
            ISerializer serializer)
            : base(loggerFactory, cacheClientFactory, serializer)
        {
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}