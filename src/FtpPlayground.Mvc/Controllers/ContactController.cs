﻿using FtpPlayground.Caching;
using FtpPlayground.Logging;
using FtpPlayground.Serialization;
using System.Web.Mvc;

namespace FtpPlayground.Mvc.Controllers
{
    public class ContactController : AbstractController
    {
        public ContactController(
            ILoggerFactory loggerFactory,
            ICacheClientFactory cacheClientFactory,
            ISerializer serializer)
            : base(loggerFactory, cacheClientFactory, serializer)
        {
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}