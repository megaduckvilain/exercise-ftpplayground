﻿using System;

namespace FtpPlayground.Caching.Memory
{
    /// <summary>
    /// Factory pattern implementation for <see cref="ICacheClient"/>.
    /// </summary>
    public class CacheClientFactory : ICacheClientFactory
    {
        #region >>>> IDisposable

        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _cacheClient.Value.Dispose();
            }

            _disposed = true;
        }

        #endregion

        private readonly Lazy<ICacheClient> _cacheClient = new Lazy<ICacheClient>(() =>
        {
            return new CacheClient();
        }, true);

        public CacheClientFactory()
        {
        }

        public ICacheClient GetCacheClient()
        {
            return _cacheClient.Value;
        }
    }
}
