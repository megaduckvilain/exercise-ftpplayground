﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace FtpPlayground.Caching.Memory
{
    /// <summary>
    /// Implementation of <see cref="ICacheClient"/> using Microsoft's System.Runtime.Caching library. 
    /// </summary>
    public sealed class CacheClient : ICacheClient
    {
        #region >>>> IDisposable

        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _cache.Dispose();
            }

            _disposed = true;
        }

        #endregion

        private readonly MemoryCache _cache;

        public CacheClient()
        {
            // Use the default collection for our cache
            _cache = MemoryCache.Default;
        }

        public bool Add(string key, byte[] value)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            var cacheItem = new CacheItem(key, value);

            return _cache.Add(cacheItem, new CacheItemPolicy());
        }

        public byte[] AddOrGetExisting(string key, byte[] value)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            var cacheItem = new CacheItem(key, value);
            var existingCacheItem = _cache.AddOrGetExisting(cacheItem, new CacheItemPolicy());

            return existingCacheItem?.Value as byte[];
        }

        public bool AddOrSet(string key, byte[] value)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            var cacheItem = new CacheItem(key, value);

            if (!_cache.Contains(key))
            {
                return _cache.Add(cacheItem, new CacheItemPolicy());
            }
            else
            {
                _cache.Set(cacheItem, new CacheItemPolicy());
                return true;
            }
        }

        public bool Contains(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException(nameof(key));
            }

            return _cache.Contains(key);
        }

        public byte[] Get(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException(nameof(key));
            }

            return _cache.Get(key) as byte[];
        }

        public IDictionary<string, byte[]> GetValues(IEnumerable<string> keys)
        {
            if (keys == null || !keys.Any())
            {
                throw new ArgumentNullException(nameof(keys));
            }

            var items = _cache.GetValues(keys);

            if (items == null || !items.Any())
            {
                return null;
            }

            return items
                .Select(x => new KeyValuePair<string, byte[]>(x.Key, x.Value as byte[]))
                .ToDictionary(x => x.Key, x => x.Value);
        }

        public byte[] Remove(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException(nameof(key));
            }

            return _cache.Remove(key) as byte[];
        }

        public void Set(string key, byte[] value)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            var cacheItem = new CacheItem(key, value);

            _cache.Set(cacheItem, new CacheItemPolicy());
        }
    }
}
