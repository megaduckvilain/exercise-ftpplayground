﻿using System;
using System.Collections.Generic;

namespace FtpPlayground.DownloadManager
{
    /// <summary>
    /// Represents a simple download manager that can asynchronously download multiple files from an FTP server.
    /// </summary>
    public interface IDownloadManager : IDisposable
    {
        /// <summary>
        /// Starts downloading specified file in the background.
        /// </summary>
        /// <returns>
        /// Returns a unique id for the download.
        /// </returns>
        Guid StartDownload(string ftpUri, string ftpUsername, string ftpPassword, string filename, DownloadProgressDelegate downloadProgressCallback, DownloadCompletedDelegate downloadCompletedCallback);

        /// <summary>
        /// Cancels specified download.
        /// </summary>        
        void CancelDownload(Guid downloadId, DownloadCancelledDelegate downloadCancelledCallback);

        /// <summary>
        /// Gets a list of all pending downloads.
        /// </summary>        
        IEnumerable<Guid> GetPendingDownloads();

        /// <summary>
        /// Cancels all pending downloads.
        /// </summary>
        /// <param name="downloadCancelledCallback"></param>
        void CancelPendingDownloads(DownloadCancelledDelegate downloadCancelledCallback);
    }

    /// <summary>
    /// Gets called every time a new block of bytes from a download is processed and written to disk.
    /// </summary>
    public delegate void DownloadProgressDelegate(Guid downloadId, double downloadRatePerSecond, double downloadedBytes, double fileSize, TimeSpan timeRemaining);

    /// <summary>
    /// Gets called once a download is complete and the file has been written to disk.
    /// </summary>
    public delegate void DownloadCompletedDelegate(Guid downloadId, TimeSpan timeTaken, string filePath, bool hasError, Exception errorDetails);

    /// <summary>
    /// Gets called once a download is cancelled.
    /// </summary>
    public delegate void DownloadCancelledDelegate(Guid downloadId);
}
