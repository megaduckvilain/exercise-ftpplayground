﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web.Hosting;

namespace FtpPlayground.DownloadManager
{
    /// <summary>
    /// Basic implementation of <see cref="IDownloadManager"/> that is capable of downloading multiple files asynchronously in the background.
    /// </summary>    
    public class DownloadManager : IDownloadManager
    {
        #region >>>> IDisposable

        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                // We need to raise cancellation requests and dispose of all our cancellation sources
                if (_cancellationTokenSources.Any())
                {
                    foreach (var cancelationTokenSource in _cancellationTokenSources.Values)
                    {
                        if (cancelationTokenSource == null)
                        {
                            continue;
                        }

                        cancelationTokenSource.Cancel();
                        cancelationTokenSource.Dispose();
                    }
                }
            }

            _disposed = true;
        }

        #endregion

        /// <summary>
        /// Keeps track of all <see cref="CancellationTokenSource"/>s that have been created for pending downloads.
        /// </summary>
        private readonly ConcurrentDictionary<Guid, CancellationTokenSource> _cancellationTokenSources = new ConcurrentDictionary<Guid, CancellationTokenSource> { };

        /// <summary>
        /// Keeps track of download speeds for pending downloads.
        /// </summary>
        private readonly ConcurrentDictionary<Guid, Queue<(DateTime timestamp, double bytes)>> _downloadProgressQueues = new ConcurrentDictionary<Guid, Queue<(DateTime timestamp, double bytes)>> { };

        /// <summary>
        /// Interval at which <see cref="DownloadProgressDelegate"/> should be called for pending dowloads.
        /// </summary>
        private readonly TimeSpan _downloadProgressUpdateInterval;

        /// <summary>
        /// Path to a directory that will be used to store partially downloaded files.
        /// </summary>
        private readonly string _workingDirectoryPath;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="workingDirectoryPath">Path to a directory where partial and completed downloads should be stored.</param>
        /// <param name="downloadProgressUpdateInterval">Interval at which <see cref="DownloadProgressDelegate"/> should be called for pending dowloads.</param>
        public DownloadManager(string workingDirectoryPath, TimeSpan downloadProgressUpdateInterval)
        {
            // Validate parameters
            if (string.IsNullOrWhiteSpace(workingDirectoryPath))
            {
                throw new ArgumentNullException(nameof(workingDirectoryPath));
            }

            if (downloadProgressUpdateInterval.TotalMilliseconds <= 0D)
            {
                throw new ArgumentOutOfRangeException(nameof(downloadProgressUpdateInterval));
            }

            // Create working directory
            try
            {
                Directory.CreateDirectory(workingDirectoryPath);
            }
            catch (Exception ex)
            {
                // TODO: Log
                throw;
            }

            _workingDirectoryPath = workingDirectoryPath;
            _downloadProgressUpdateInterval = downloadProgressUpdateInterval;
        }

        public Guid StartDownload(string ftpUri, string ftpUsername, string ftpPassword, string filename, DownloadProgressDelegate downloadProgressCallback, DownloadCompletedDelegate downloadCompletedCallback)
        {
            // Validate parameters
            if (string.IsNullOrWhiteSpace(ftpUri))
            {
                throw new ArgumentNullException(nameof(ftpUri));
            }

            if (string.IsNullOrWhiteSpace(ftpUsername))
            {
                throw new ArgumentNullException(nameof(ftpUsername));
            }

            if (string.IsNullOrWhiteSpace(ftpPassword))
            {
                throw new ArgumentNullException(nameof(ftpPassword));
            }

            var downloadId = Guid.NewGuid();

            // Start download in the background
            HostingEnvironment.QueueBackgroundWorkItem(async systemCancellationToken =>
            {
                var workingFilename = Path.Combine(_workingDirectoryPath, $"file-{downloadId}.part");
                var fileSize = 0D;
                var timeStarted = DateTime.UtcNow;

                try
                {
                    // Build request uri
                    var requestUri = new Uri(Path.Combine(ftpUri, filename), UriKind.Absolute);

                    #region >>>> Get file size from FTP server
                    {
                        var request = WebRequest.Create(requestUri) as FtpWebRequest;
                        request.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                        request.Method = WebRequestMethods.Ftp.GetFileSize;

                        using (var response = await request.GetResponseAsync() as FtpWebResponse)
                        {
                            // Check response
                            if (response == null)
                            {
                                // TODO: Log
                                downloadCompletedCallback?.Invoke(downloadId, TimeSpan.Zero, null, true, new InvalidOperationException("FTP server response is null."));
                            }

                            if (response.StatusCode != FtpStatusCode.FileStatus)
                            {
                                downloadCompletedCallback?.Invoke(downloadId, TimeSpan.Zero, null, true, new InvalidOperationException($"FTP server response does not indicate success. Message: {response.StatusDescription}"));
                            }

                            fileSize = response.ContentLength;
                        }
                    }
                    #endregion

                    #region >>>> Download file from FTP server
                    {
                        var request = WebRequest.Create(requestUri) as FtpWebRequest;
                        request.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                        request.Method = WebRequestMethods.Ftp.DownloadFile;

                        using (var response = await request.GetResponseAsync() as FtpWebResponse)
                        {
                            // Check response
                            if (response == null)
                            {
                                // Invoke callback
                                downloadCompletedCallback?.Invoke(downloadId, TimeSpan.Zero, null, true, new InvalidOperationException("FTP server response is null."));
                                return;
                            }

                            if (!(response.StatusCode == FtpStatusCode.OpeningData ||
                                response.StatusCode == FtpStatusCode.ClosingData))
                            {
                                // Invoke callback
                                downloadCompletedCallback?.Invoke(downloadId, TimeSpan.Zero, null, true, new InvalidOperationException($"FTP server response does not indicate success. Message: {response.StatusDescription}"));
                                return;
                            }

                            // Init download progress queue
                            var downloadProgressQueue = _downloadProgressQueues.GetOrAdd(downloadId, new Queue<(DateTime timestamp, double bytes)> { });

                            // Create a linked cancellation token source that will allow us to cancel downloads
                            var cancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(systemCancellationToken);
                            _cancellationTokenSources.TryAdd(downloadId, cancellationTokenSource);

                            // Get a new cancellation token
                            var cancellationToken = cancellationTokenSource.Token;

                            // Process response 
                            #region ...

                            // Open response stream
                            using (var downloadStream = response.GetResponseStream())
                            {
                                // Open file stream
                                using (var fileStream = new FileStream(workingFilename, FileMode.Append, FileAccess.Write))
                                {
                                    // Init tracking variables
                                    var lastProgressUpdateTime = DateTime.UtcNow;
                                    var downloadedBytes = 0D;

                                    // TODO: Add size to configuration file
                                    var bufferSize = 1024;

                                    // Download file chunk and once it is downloaded write it to disk.
                                    while (true)
                                    {
                                        // Check if the download has been cancelled.
                                        cancellationToken.ThrowIfCancellationRequested();

                                        var buffer = new byte[bufferSize];

                                        // Download chunk
                                        var bytesRead = await downloadStream.ReadAsync(buffer, 0, buffer.Length);

                                        // Check if there is anything to write
                                        if (bytesRead == 0)
                                        {
                                            break;
                                        }

                                        // TODO: 
                                        // This could be refactored to write to disk in parallel with the download. We can store the previously downloaded 
                                        // buffers in a queue and flush them down to disk at our own pace without being blocked by the download.

                                        // Write chunk to disk                                                                                
                                        await fileStream.WriteAsync(buffer, 0, bytesRead);

                                        // Update progress queue
                                        var now = DateTime.UtcNow;
                                        downloadProgressQueue.Enqueue((now, bytesRead));

                                        // Update tracking variables
                                        downloadedBytes += bytesRead;

                                        // Check if we should invoke the progress callback
                                        var timeElapsed = now - lastProgressUpdateTime;

                                        if (timeElapsed >= _downloadProgressUpdateInterval)
                                        {
                                            // Calculate progress
                                            var downloadRatePerSecond = CalculateDowloadRatePerSecond(downloadProgressQueue.ToList());
                                            var downloadTimeRemaining = CalculateDowloadTimeRemaining(downloadRatePerSecond, downloadedBytes, fileSize);

                                            // Update tracking variables
                                            lastProgressUpdateTime = now;

                                            // Invoke callback
                                            downloadProgressCallback?.Invoke(downloadId, downloadRatePerSecond, downloadedBytes, fileSize, downloadTimeRemaining);

                                            // Clear queue
                                            downloadProgressQueue.Clear();
                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                        // Invoke callback
                        downloadCompletedCallback?.Invoke(downloadId, DateTime.UtcNow - timeStarted, workingFilename, false, null);
                    }
                    #endregion

                    #region >>>> Cleanup

                    // Remove cancellation token sources for this download                    
                    _cancellationTokenSources.TryRemove(downloadId, out var source);

                    // Remove download progress queue
                    _downloadProgressQueues.TryRemove(downloadId, out var queue);

                    #endregion
                }
                catch (OperationCanceledException ex)
                {
                    // TODO: Log

                    // Cleanup                    
                    _cancellationTokenSources.TryRemove(downloadId, out var source);
                    _downloadProgressQueues.TryRemove(downloadId, out var queue);
                    return;
                }
                catch (Exception ex)
                {
                    // TODO: Log

                    // Cleanup                    
                    _cancellationTokenSources.TryRemove(downloadId, out var source);
                    _downloadProgressQueues.TryRemove(downloadId, out var queue);

                    // Invoke callback
                    downloadCompletedCallback?.Invoke(downloadId, TimeSpan.Zero, null, true, ex);
                    return;
                }
            });

            return downloadId;
        }

        public void CancelDownload(Guid downloadId, DownloadCancelledDelegate downloadCancelledCallback)
        {
            // TODO
        }

        public void CancelPendingDownloads(DownloadCancelledDelegate downloadCancelledCallback)
        {
            // TODO
        }

        public IEnumerable<Guid> GetPendingDownloads()
        {
            // TODO
            return null;
        }

        /// <summary>
        /// Calculates download rate per second in bytes.
        /// </summary>        
        private double CalculateDowloadRatePerSecond(List<(DateTime timestamp, double bytes)> downloadedChunks)
        {
            if (downloadedChunks == null || !downloadedChunks.Any() || downloadedChunks.Count <= 1)
            {
                return 0D;
            }

            // Calculate the length of time it took to download all chunks
            var totalSeconds = (downloadedChunks.Last().timestamp - downloadedChunks.First().timestamp).TotalSeconds;

            // Calculate total bytes dowloaded
            var totalBytes = downloadedChunks.Sum(x => x.bytes);

            if (totalSeconds == 0D || totalBytes == 0D)
            {
                return 0D;
            }

            return totalBytes / totalSeconds;
        }

        /// <summary>
        /// Calculates remaining download time.
        /// </summary>        
        private TimeSpan CalculateDowloadTimeRemaining(double downloadRatePerSecond, double downloadedBytes, double fileSize)
        {
            if (downloadRatePerSecond <= 0 || downloadedBytes == fileSize)
            {
                return TimeSpan.Zero;
            }

            var bytesRemaining = Math.Max(fileSize - downloadedBytes, 0D);

            return bytesRemaining > 0
                ? TimeSpan.FromSeconds(bytesRemaining / downloadRatePerSecond)
                : TimeSpan.Zero;
        }
    }
}
