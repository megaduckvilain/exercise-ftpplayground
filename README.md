# FTP Playground

>Simple ASP.Net based application that indexes files on an FTP server and lets users choose files they wish to download.

## Requirements
We are to build an application which will be able to download a file from an FTP server. The application should display the following information for the file being downloaded:

- Progress bar with the state of the transfer
- Transfer rate
- Estimated time left
- Downloaded bytes
  
We should also add support for asynchronous copying of downloaded streams to disk.

## Review
For review purposes I've listed below files and projects which are most relevant to the requirements above. There is a bit of MVC boilerplate clutter all over the place, however, the choice to use a more UI driven framework was primarily based on the fact that the original target platform was Windows Forms.

Great alternative to this would have been a lightweight console app with some StdOut logging to show transfer state and so on. This would have saved some time perhaps.

Core implementation:

- ..\exercise-ftpplayground\src\FtpPlayground.Mvc\Classes\AbstractController.cs
- ..\exercise-ftpplayground\src\FtpPlayground.Mvc\Controllers\ExplorerController.cs
- ..\exercise-ftpplayground\src\FtpPlayground.Mvc\Utilities\Utils.FormatDownload***.cs
- ..\exercise-ftpplayground\src\FtpPlayground.DownloadManager
- ..\exercise-ftpplayground\src\FtpPlayground.Caching.Memory
- ..\exercise-ftpplayground\src\FtpPlayground.Serialization

## User's Manual

In this section you will find detailed instructions on how to use this application.
Each topic herein relates to a specific page of the application. So, if you are having trouble figuring out how something works on a page just jump to the section with the name of that page and read on.

### Explorer

This page contains the FTP browser and let's users download files from any FTP server they may have access to. To use the browser you will need:

- URL of the FTP server
- Username
- Password

Below are step-by-step instructions on how to use the browser.

#### Step 1: Enter FTP server details
#### Step 2: Fetch directory listing
#### Step 3: Download files
#### Step 4: Browse directories
_This feature is not yet functional._

### About

This page explains some of the design choices I made with my implementation. It also talks a little bit about the technologies I used and the general approach to developing the features required.

### Contact

This page lists my contact details as well as the location of source code repo. Not that anyone would be interested in contributing but it's still nice to imagine people are staring. Keeps you from writing something stupid :)

## Developer's Journal

In this section you will find technical information relating to the implementation details of all major modules of this application.

### 3rd Party Components
I've used a number of open-source components in this application. Here is a list of all components I've added (updated) in addition to those that came with the MVC 5 VisualStudio template from Microsoft.
  
- [Unity Container](https://github.com/unitycontainer/unity)
- [protobuf-net](https://github.com/mgravell/protobuf-net)
- [Bootstrap](https://getbootstrap.com/docs/3.3/)
- [Glyphicons](https://www.glyphicons.com/)
- [Loading.io](https://loading.io/button/)
- [SignalR](https://github.com/SignalR/SignalR)
- [ByteSize](https://github.com/omar/ByteSize)

### CSS
I've added some of my own CSS classes to make things a little prettier and concise on certain pages.
All new styles as well as overrides for some of the base Bootstrap ones can be found here:
>..\src\FtpPlayground.Mvc\Content\custom.css

### FTP Servers
As it turns out FTP servers don't like standards, and even if they did they don't follow them. Having gone through a whole bunch of test servers out there I found that not a single one was alike.

I also came across this [document](https://files.stairways.com/other/ftp-list-specs-info.txt) written by one Mr. Peter Lewis. 

_I would like to point out I was still in highschool when he wrote it._

The document goes into great detail as to how badly we needed a standard for these things way back when. I was glad to find out that to this day not much has changed. So, to keep my implementation simple and compatible to the largest degree possible I opted to rely on **NLIST** command when fetching directory listings.
The upshot is that most servers now return meaningful results, but the downside is that we only get to see file and directory names.

This is something definitely worth messing around with in the future.

### Dependency Injection & Abstractions

Throughout this application I rely on certain unimplemented functionality that was either out of scope or there simply wasn't much free time to do it.

However, I felt that it was appropriate to indicate where and how I intended to use said functionality. So, where applicable I've provided dummy implementations of various abstractions such as Logging, Caching and so on.

Eventually I'll get around to doing it but until that time let's pretend it just works.

### SignalR & Asynchronous Downloads

In order to keep things simple and manageable when dealing with simultaneous downloads I've put all logic pertaining to downloading files into a separate manager class.

This *DownloadManager* takes care of the entire download cycle and is also responsible for flushing downloaded stream to disk. In order to keep UI and other parts of the system up-to-date on the progress made as well as general status of each download I make use of delegates.

Essentially the entire thing is callback driven and this helps to keep things simple on the controller side as well. This is where **SignalR** comes in.

Whenever there is an update on one of the callbacks I use **SignalR** to send those updates to the client. On the client side things are even simpler and I just use **jQuery** to update those HTML elements which are related to the files being downloaded.

The *DownloadManager* handles all downloads in parallel and also fully supports download cancellations however this functionality was out of scope so currently remains unfinished.

### IIS & Long Running Tasks

One of the more subtle issues with current implementation is that IIS will need a bit of configuration love in order not to tear down the application while it is busy downloading larger files.
Other solution would be to use a job system like [Hangfire](https://www.hangfire.io/) or [Quartz.NET](https://github.com/quartznet/quartznet) or I could have my own service running in the background and so on.

Various solutions to this problem are out there but then again it is a bit out of scope for now.

### Security

This is an important issue to address and to be honest very little effort was made on my part to implement this application with security in mind.

In the future the following issues would definitely need attention:

- Use of _SecureString_ class whenever handling credentials
- User accounts for the application itself
- Tokenization of requests to minimize risk of compromised requests
- Audit logging of all actions taken by users

### Tests

Although testing is out of scope for this exercise it is an important aspect to consider in any project. I've tried to use many abstractions and keep main logic isolated from the rest of the MVC pipeline.

In the future, if time permits, it will be very easy to add unit tests to the solution.

### Error Handling

Again due to time constraints error handling and general feedback to the user is kept to a bare minimum. This is yet another part of the application that will need additional work in order to smooth out user experience when bad things happen.

### Nice-To-Haves

Bunch of ideas to push this application a bit further:

- Ability to cancel downloads
- Ability to pause / resume downloads
- Ability to choose file download locations and names

***
Written by **Alex Pavlovsky** with lots of <3, C#, JS, CSS, HTML and a bit of markdown.